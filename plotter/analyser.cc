#include <iostream>
#include <fstream>
#include <cmath>
#include <stdint.h>
#include <string>
#include <vector>
#include <stdlib.h>
#include <cstdio>
#include <signal.h>

#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TBenchmark.h"
#include "TSystem.h"
#include "TTimeStamp.h"


#define TIME 120		//time in second for histogram range
#define BW 250			//bandwidth in Gbit/s for histograms range
#define us 1000000		//microseconds to second
#define Gbit 1000000000		//bit to Gbit
#define bit 8			//byte to bit

using namespace std;

void analyser( string run_name )
{		
	//common variables	
	string run_name_list = run_name;					//variable for input file name	
	run_name_list.append(".txt");
	string size_s, not_step_s, data_file, offset_s, task_start_time_s;	//variables to read from data file=> blocksize (Byte), #of measurements, data file name, offset (Byte), time since epoch (us)
	string loop_s;								//variable to keep # of blocksize read in a cycle
	int run_start_time, run_end_time, run_max_time;			//time information of tasks
	string file_name, run_name_temp, path;					//variables to open task output file
	size_t found, found_2;							//variables to string search

	//TRAINING
	cout << "Training has succesfully started" << endl;
	//variables
	int max_nodeid=0, max_taskid=0, max_taskid_tmp=0, max_nodeid_tmp=0;	//variables to find max node and task number
	vector <double> train_time_start;					//vector to keep start times 
	vector <double> train_time_end;						//vector to keep end times
	int sec_count;								//second counter

	//opening run list
	ifstream training_file( run_name_list, ios::in );
        if ( !training_file.is_open() )
        {
                cout << run_name_list << " cannot be opened. No such file or directory" << endl;
                return 0;
        }
	
	//looping over run list	
	while ( !training_file.eof() )
	{
		//getting run names and check if the run name is empty or incudes "#" character
		//names include "#" character are omitted
		getline( training_file, file_name );
		if ( file_name.empty() ) continue;
		else if ( file_name.find("#") != std::string::npos ) continue;
		
		//finding max task and mask node number
		found = file_name.find("_");
                if ( found != std::string::npos )
                {
                 	found_2 = file_name.find(".txt");
                        max_nodeid_tmp = stoi( file_name.substr( found + 1, 1) );
                    	max_taskid_tmp = stoi( file_name.substr( found + 3, found_2 - ( found + 3) ) );
			if( max_nodeid_tmp > max_nodeid )
				max_nodeid = max_nodeid_tmp;
			if ( max_taskid_tmp > max_taskid )
				max_taskid = max_taskid_tmp;
                }

		//reinitializing contents of the some variable before looping over run files
		run_name_temp = run_name;
		path = "./";
		
		//opening a task output file
		ifstream train_file( path.append( run_name_temp.append("/").append( file_name.c_str() ) ), ios::in);
  		if ( !train_file.is_open() )
        	{
                	cout << file_name << " cannot be opened. No such file or directory" << endl;
                	return 0;
        	}

		//reading firstline
		train_file >> size_s >> not_step_s >> data_file >> offset_s >> task_start_time_s;
		train_time_start.push_back( atof( task_start_time_s.c_str() ) / us );	 	//start time since epoch in seconds for each task in a run
		
		//looping over the lines of the task output file to count the number of measurements (in other words length of measurements in seconds)
		sec_count = 0;
		while ( train_file >> loop_s )
		{
			if ( loop_s == "-1" ) break;
			sec_count++;
		}
		train_time_end.push_back( sec_count + atof( task_start_time_s.c_str() ) / us);	//end time since epoch in seconds for each task in a run
	}
	train_time_end.pop_back();								//dropping the last entry since it is trivial
	
	//caclculating the time of latest starting task as run_start_time and the time of earliest ending task as run_end_time
	run_start_time = (int) *max_element( train_time_start.begin(), train_time_start.end() );
	run_end_time = (int) *min_element( train_time_end.begin(), train_time_end.end() );
	run_max_time = run_end_time - run_start_time - 2;						//maximum run length for the run
	//cout << run_max_time << endl;	

	cout << "Training has succesfully finished and starting the analysis now" << endl;
	
	//ANALYSIS
	//variables
	string run_name_out = run_name;							//variable for output file
	run_name_out.append("_output.txt");
	
	string run_name_root = run_name;						//variable for output root file
	run_name_root.append(".root");

	int jobid, taskid, nodeid, loop, i, max_delay;						//some numarical variables needed for analysis
	double size, not_step, task_start_time, delay, rate, integral;
	
	TH1F style( "nodeid", "micro benchmark", TIME, 0, TIME );			//declaring histograms for each task
	TH1F *node_total[ max_nodeid + 1 ];
	for ( int i=0; i < max_nodeid + 1; i++ )
		node_total[ i ] = (TH1F*)style.Clone( Form( "node_%d", i ) );
	
	string run_name_total = run_name;                                                 
        run_name_total.append("total");
	TH1F *total = new TH1F( run_name_total.c_str(), run_name_total.c_str(), TIME, 0, TIME );		//declaration of a histogram for run total vs time
	string run_name_total_distro = run_name;                                                
        run_name_total_distro.append("total_distro");
	TH1F *total_distro = new TH1F( run_name_total_distro.c_str(), run_name_total_distro.c_str(), BW, 0, BW );		//declaration of a histogram for distrubution of run total
	
	//creation of output file
	ofstream output_file( run_name_out, ios::out );
  	if ( !output_file.is_open() )
        {
                cout << run_name_out << " cannot be created. Check permissions" << endl;
                return 0;
        }

	//opening of input file
 	ifstream input_file( run_name_list, ios::in );	
  	if ( !input_file.is_open() )
	{
		cout << run_name_list << " cannot be opened. No such file or directory" << endl;
		return 0;
	}
	
	//creation of root output file
	TFile * Results_File = new TFile( run_name_root.c_str(), "recreate" );
	if ( !Results_File->IsOpen() )
        {
                cout << Results_File << " cannot be created. Check permissions" << endl;
                return 0;
        }
	
	//looping over input file list (tasks list)
	while ( !input_file.eof() )
	{
		getline( input_file, file_name);
		TH1F *task = new TH1F( file_name.c_str(), file_name.c_str(), TIME, 0, TIME );
		
		//closing the file if eof
		if ( file_name.empty() )
		{		
			//histogram formatting and writing
			total->GetYaxis()->SetTitle("Rate (Gbit/s)");
  			total->GetXaxis()->SetTitle("Time (sec)");
  			total->GetXaxis()->SetTitleSize(0.045);
  			total->GetXaxis()->SetLabelSize(0.045);
  			total->GetYaxis()->SetTitleSize(0.045);
 	 		total->GetYaxis()->SetLabelSize(0.045);
  			total->SetLineColor(kRed);
  			total->SetLineWidth(2);
  			total->SetMarkerColor(kBlue);
  			total->SetMarkerStyle(7);
			total->Write();
		
			integral = total->Integral(1,run_max_time);	
			//filling a distribution histogram
			//integral = 0;
			for( int i = 1; i < run_max_time+1; i++ ) 
			{
				total_distro->Fill( total->GetBinContent(i) );
				//integral += total->GetBinContent(i);
				//cout<<total->GetBinContent(i)<<endl;
			}
			total_distro->GetYaxis()->SetTitle("# of entries");
                        total_distro->GetXaxis()->SetTitle("Throughput (Gbit/s)");
                        total_distro->GetXaxis()->SetTitleSize(0.045);
                        total_distro->GetXaxis()->SetLabelSize(0.045);
                        total_distro->GetYaxis()->SetTitleSize(0.045);
                        total_distro->GetYaxis()->SetLabelSize(0.045);
                        total_distro->SetLineColor(kRed);
                        total_distro->SetLineWidth(2);
                        total_distro->SetMarkerColor(kBlue);
                        total_distro->SetMarkerStyle(7);
			total_distro->Write();
			
			//formatting and writing node total histograms
			for( int i = 0; i <= max_nodeid; i++ )
			{
				node_total[i]->GetYaxis()->SetTitle("Rate (Gbit/s)");
  				node_total[i]->GetXaxis()->SetTitle("Time (sec)");
  				node_total[i]->GetXaxis()->SetTitleSize(0.045);
  				node_total[i]->GetXaxis()->SetLabelSize(0.045);
 				node_total[i]->GetYaxis()->SetTitleSize(0.045);
 	 			node_total[i]->GetYaxis()->SetLabelSize(0.045);
  				node_total[i]->SetLineColor(kRed);
  				node_total[i]->SetLineWidth(2);
  				node_total[i]->SetMarkerColor(kBlue);
  				node_total[i]->SetMarkerStyle(7);
				node_total[i]->Write();
			}
			
				
			//cout << max_delay << endl;
			//write an output file
			cout << fixed << \
				"RUN:\t\t" << run_name << "\n" << \
				"FILE:\t\t" << data_file << "\n" << \
				"BLOCKSIZE:\t" << size_s << " Byte\n" << \
				"OFFSET:\t\t" << offset_s << " Byte\n" << \
				"MAX TIME:\t" << not_step_s << " second\n" << \
				"NODE COUNT:\t" << max_nodeid+1 << "\n" << \
				"TASK COUNT:\t" << max_taskid+1 << "\n" << \
				"RUN START TIME:\t" << run_start_time << "\n" << \
				"RUN END TIME:\t" << run_end_time << "\n" << \
				"RUN TIME:\t" << run_max_time << " second\n" << \
				"MAX DELAY:\t" << max_delay << " second\n" << \
				"TOTAL SIZE:\t" << integral << " Gbit\n" << \
				"RATE:\t\t" << integral / run_max_time << " Gbit/s\n" << \
				"MEAN:\t\t" << total_distro->GetMean() << " Gbit/s\n" << \
				"MEAN ERROR:\t" << total_distro->GetMeanError() << " Gbit/s\n" << \
				"STDDEV:\t\t" << total_distro->GetStdDev() << "\n" << \
				"MAX \t\t" << total->GetMaximum() << " Gbit/s\n" << \
				"MIN \t\t" << total->GetMinimum(1) << "Gbit/s" << endl;
			
			output_file <<fixed << \
				run_name << "\t" << \
				data_file << "\t" << \
				size_s << "\t" << \
				offset_s << "\t" << \
				not_step_s << "\t" << \
				max_nodeid+1 << "\t" << \
				max_taskid+1 << "\t" << \
				run_start_time << "\t" << \
				run_end_time << "\t" << \
				run_max_time << "\t" << \
				max_delay << "\t" << \
				integral << "\t"<< \
				integral/ run_max_time << "\t" << \
				total_distro->GetMean() << "\t" << \
				total_distro->GetMeanError() << "\t" << \
				total_distro->GetStdDev() << "\t" << \
				total->GetMaximum() << "\t" << \
				total->GetMinimum(1) << endl;
			
			//closing and exiting the program	
			input_file.close();
			output_file.close();
			Results_File->Close();
			cout << "Analysis has successfully finished. Exiting" << endl << endl;
			exit(1);
		}
	
		//fetching jobid, taskid, nodeid	
		found = file_name.find("_");
                if ( found != std::string::npos )
                {
                 	found_2 = file_name.find(".txt");
                        jobid = stoi( file_name.substr( found - 6, 6 ) );
                        nodeid = stoi( file_name.substr( found + 1, 1 ) );
                    	taskid = stoi( file_name.substr( found + 3, found_2 - ( found + 3 ) ) );
                }
		
		//opening a task file
		run_name_temp = run_name;
		path = "./";
		ifstream file( path.append( run_name_temp.append("/").append( file_name.c_str() ) ), ios::in );
  		if (!file.is_open())
        	{
                	cout << file_name << " cannot be opened. No such file or directory" << endl;
                	return 0;
        	}
		
		//reading firstline
		file >> size_s >> not_step_s >> data_file >> offset_s >> task_start_time_s;
		size = atof( size_s.c_str() );					//blocksize in bytes
		not_step = atof( not_step_s.c_str() );				//the length of measurements set prior the run
		task_start_time = atof( task_start_time_s.c_str() ) / us;	//task start time since epoch in seconds
		
		delay = run_start_time - task_start_time;			//delay in task start time wrt run start time
		if ( delay > max_delay) max_delay = delay;		

		//looping over the lines of task output file
		i = 0;
		while (file >> loop_s)
		{	
			if ( i < delay || i > (run_max_time + delay ) ) { i++; continue;}
			loop = atoi( loop_s.c_str() );				//number of blocksize read in a second
			rate = ( size * loop * bit) / Gbit;			//rate in Gbit/s
			task->SetBinContent(i - delay, rate);
			i++;			
		}
		
		node_total[ nodeid ]->Add( task );				//summing task for the same node
		total->Add( task );						//summing tasks for the same run
  		task->GetYaxis()->SetTitle("Rate (Gbit/s)");
  		task->GetXaxis()->SetTitle("Time (sec)");
  		task->GetXaxis()->SetTitleSize(0.045);
  		task->GetXaxis()->SetLabelSize(0.045);
  		task->GetYaxis()->SetTitleSize(0.045);
 	 	task->GetYaxis()->SetLabelSize(0.045);
  		task->SetLineColor(kBlue);
  		task->SetLineWidth(2);
  		task->SetMarkerColor(kBlue);
  		task->SetMarkerStyle(7);
  		task->Write();
		task->Delete();
		file.close();
	}
}
