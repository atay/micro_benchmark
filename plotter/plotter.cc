#include <iostream>
#include <fstream>
#include <cmath>
#include <stdint.h>
#include <string>
#include <vector>
#include <stdlib.h>
#include <cstdio>
#include <signal.h>

#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TBenchmark.h"
#include "TSystem.h"
#include "TTimeStamp.h"

#define MAX_NODE 5

using namespace std;

void plotter( string run_name )
{		
	//common variables	
	string run_name_list = run_name;					//variable for input file name	
	string file_name;					//variables to open task output file
	string run_s, file_s, size_s, offset_s, max_time_s, node_count_s, task_count_s, run_start_time_s, run_end_time_s, run_time_s, max_delay_s, total_size_s, rate_s, mean_s, mean_error_s, stddev_s, max_s, min_s;
	int size, offset, max_time, node_count, task_count, run_start_time, run_end_time, run_time, max_delay;
	double total_size, rate, mean, mean_error, stddev, max, min;
	vector <double> tp[MAX_NODE], task[MAX_NODE], tperr[MAX_NODE], tpdev[MAX_NODE];
	//cout << "Training has succesfully started" << endl;
	int max_node = 0;

	string stride = run_name.substr(run_name.length()-1,1);
	string off = run_name.substr(run_name.length()-3,1);
	string blocksize = run_name.substr(run_name.length()-5,1);
	//opening run list
	ifstream training_file( run_name_list.c_str(), ios::in );
        if ( !training_file.is_open() )
        {
                cout << run_name_list << " cannot be opened. No such file or directory" << endl;
                return 0;
        }
	
	//looping over run list	
	while ( !training_file.eof() )
	{
		//getting run names and check if the run name is empty or incudes "#" character
		//names include "#" character are omitted
		getline( training_file, file_name );
		if ( file_name.empty() ) continue;
		else if ( file_name.find("#") != std::string::npos ) continue;
		
		file_name.append("_output.txt");
		
		//opening a task output file
		ifstream train_file( file_name.c_str()  , ios::in);
  		if ( !train_file.is_open() )
        	{
                	cout << file_name << " cannot be opened. No such file or directory" << endl;
                	return 0;
        	}

		//reading firstline
		train_file >> run_s >> file_s >> size_s >> offset_s >> max_time_s >> node_count_s >> task_count_s >> run_start_time_s >> run_end_time_s >> run_time_s >> max_delay_s >> total_size_s >> rate_s >>mean_s >> mean_error_s >> stddev_s >> max_s >> min_s;
		size = atoi (size_s.c_str());
		offset = atoi (offset_s.c_str());
		max_time = atoi (max_time_s.c_str());
		node_count = atoi (node_count_s.c_str());
		task_count = atoi (task_count_s.c_str());
		run_start_time = atoi (run_start_time_s.c_str());
		run_end_time = atoi (run_end_time_s.c_str());
		run_time = atoi (run_time_s.c_str());
		max_delay = atoi (max_delay_s.c_str());
		total_size = atof (total_size_s.c_str());
		rate = atof (rate_s.c_str());
		mean = atof (mean_s.c_str());
		mean_error = atof (mean_error_s.c_str());
		stddev = atof (stddev_s.c_str());
		max = atof (max_s.c_str());
		min = atof (min_s.c_str());
		cout<<rate<<endl;
		tp[node_count-1].push_back(rate);
		tperr[node_count-1].push_back(mean_error);
		tpdev[node_count-1].push_back(stddev);
		task[node_count-1].push_back(task_count*node_count);
		if (node_count>max_node) max_node = node_count;
	//	cout << "Training has succesfully finished and starting the analysis now" << endl;
		train_file.close();
	}
	training_file.close();
	std::size_t found = run_name.find_last_of("/\\");
	//cout<<run_name.length()<<"\t"<<found<<endl;
	string run = run_name.substr(found+1,run_name.length()-found-7);
	//cout<<run<<endl;
	run.append("_").append(blocksize).append("_").append(off).append("_").append(stride).append(".root");
	//creation of root output file
	TFile * Results_File = new TFile( run.c_str(), "recreate" );
	if ( !Results_File->IsOpen() )
	{
		cout << Results_File << " cannot be created. Check permissions" << endl;
		return 0;
	}
	for (int i=0;i<max_node;i++)
	{
		TGraphErrors *GraphE = new TGraphErrors(tp[i].size(),&task[i][0],&tp[i][0], 0 , & tpdev[i][0]);	
		gStyle->SetOptFit(11111111);
    		GraphE->GetYaxis()->SetTitle("Total Throughput (Gbit/s)");
    		GraphE->GetYaxis()->SetTitleSize(0.045);
    		GraphE->GetYaxis()->SetLabelSize(0.045);
    		GraphE->GetXaxis()->SetTitle(" Total # of Clients");
    		GraphE->GetXaxis()->SetTitleSize(0.045);
    		GraphE->GetXaxis()->SetLabelSize(0.045);
    		GraphE->SetMarkerColor(kRed);
    		GraphE->SetMarkerStyle(20);
    		GraphE->SetMarkerSize(1.1);
    		GraphE->SetLineColor(kRed);
    		GraphE->SetLineStyle(3);
    		GraphE->SetLineWidth(2);
    		GraphE->SetFillColor(kWhite);
    		GraphE->SetTitle(to_string(i+1).append("_").append(blocksize).append("_").append(off).append("_").append(stride).c_str());
		GraphE->Write("C");
		
		TGraph *Graph = new TGraph(tp[i].size(),&task[i][0],&tp[i][0]);	
		gStyle->SetOptFit(11111111);
    		Graph->GetYaxis()->SetTitle("Total Throughput (Gbit/s)");
    		Graph->GetYaxis()->SetTitleSize(0.045);
    		Graph->GetYaxis()->SetLabelSize(0.045);
    		Graph->GetXaxis()->SetTitle("Total# of Clients");
    		Graph->GetXaxis()->SetTitleSize(0.045);
    		Graph->GetXaxis()->SetLabelSize(0.045);
    		Graph->SetMarkerColor(kBlue);
    		Graph->SetMarkerStyle(20);
    		Graph->SetMarkerSize(1.1);
    		Graph->SetLineColor(kBlue);
    		Graph->SetLineStyle(3);
    		Graph->SetLineWidth(2);
    		Graph->SetFillColor(kWhite);
    		Graph->SetTitle(to_string(i+1).append("_").append(blocksize).append("_").append(off).append("_").append(stride).c_str());
		Graph->Write("C");
	}	
	Results_File->Close();
	exit(1);
}
