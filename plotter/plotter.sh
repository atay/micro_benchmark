#!/bin/bash
scratchdir=$(pwd)
output=/scratch/compeng/atay/micro_benchmark_2/client/output
source /scratch/compeng/caching/root/bin/thisroot.sh

cp plotter.cc $output/
cd $output
#echo $1
root -l -b  "plotter.cc(\"${scratchdir}/$1\")"
rm ${output}/plotter.cc
cd $scratchdir
