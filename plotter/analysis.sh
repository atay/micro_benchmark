#!/bin/bash
scratchdir=$(pwd)

#define your output folder where your data are
output=/scratch/compeng/atay/micro_benchmark_2/client/output

#sourcing ROOT libraries. one can get sn own copy 
source /scratch/compeng/caching/root/bin/thisroot.sh

while IFS='' read -r line || [[ -n "$line" ]]; do

cp analyser.cc $output/
cd $output/$line
ls *.txt>& ../${line}.txt
cd ../

root -l -b  "analyser.cc(\"${line}\")"
rm analyser.cc
cd $scratchdir
done < $1
