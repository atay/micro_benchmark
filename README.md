# Benchmark for the Verbund project

## Description
A parametrized package to submit a micro-benchmark slurm job with caching to Goethe-HLR. 

## Usage
- git clone https://git.gsi.de/atay/micro_benchmark.git

-- Starting clients. Set variables in client/client bash file
- cd ../client
- sbatch client

-- Plotting the results
- cd ../plotter
- ./analysis.sh <input_file> 
    - <input_file> is the list of runs in format <job_name>_<job_ID>
    - creates output files for each run
    - creates time profile for each client in each run
- ./plotter.sh <input_file>
    - <input_file> is the list of runs in format <job_name>_<job_ID>
    - creates single file merging all the results from <input_file>. Ideal for generating plots for the same group of runs. For example, DCOTF with different number of clients.
    - it has to be called after calling analysis.sh
- root
- new TBrowser
    - Browse to output_file.root to see the plot 

## Procedure explanation
- "client" starts clients
    - output can be found in output/${SLURM_JOB_ID}. Please check micro_benchmark.cc for output format. 
- "plotter" is just a generic plotter for monitoring purposes. It is not perfect.

## Notes
- You can check availability of reserved nodes
    - squeue -w node48-[001-004]
