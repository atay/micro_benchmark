/*
 * Author: Serhat Atay
 * atay@compeng.uni-frankfurt.de
 *
 *Opens a file to read chunks from a url 
 *Required argumants to pass in order
 *
 * -./micro_benchmark
 * -url of the file to be read i.e. root://server//path or file://server//path
 * -output_file to write results
 * -blocksize in kB
 * -iteration, number of total entry in the output_file 
 * -offsett in kB
 * -stride in kB
 *
*/


#include "XrdCl/XrdClStatus.hh"
#include "XrdCl/XrdClFile.hh"
#include <iostream>
#include <string>
#include <chrono>
#include <fstream>
#include <cmath>
#include <iomanip>

#define MAX_NAME 1000
#define kB 1000
#define kiB 1024
#define MB 1000000
#define MiB 1048576
#define MAX_SIZE 53687091200		//50GB, manually checking the file end by defining the filesize here
using namespace XrdCl;
using namespace std;
using namespace std::chrono;

int main(int argc, char** argv)
{
	std::system("cat /etc/os-release");
	//char hostname[MAX_NAME], username[MAX_NAME];
	//gethostname(hostname, MAX_NAME);
	//getlogin_r(username, MAX_NAME);
	
	uint64_t iteration = stoi(argv[5]); 		//measurement cycles in second
	int total_counter = 0;				//a counter for blocksize read within a file. it is used to check the end of file
	int counter = 0;				//a counter for blocksize read wihtin an iteration, it is used to calculate rate
	int second = 0;					//an integer floored downwards as seconds wrt measurement start, when it is increased counter will be set zero	
	int i;	

	//variables for XrdCl::File::Open()
	const string path = argv[1];			//path to file i.e. root://server//path or file://server//path
	OpenFlags::Flags flags = OpenFlags::Read;
	Access::Mode mode = Access::None ;
	uint16_t timeout = 10;
	
	//variables for XrdCl::File::Read()
		//srand(duration_cast<microseconds>(system_clock::now().time_since_epoch()).count());
		//uint64_t offset = rand() % size;		//offset from the beginning of the file
		//uint64_t stride = rand() % size;;	
	uint64_t offset = stoi(argv[6]) * kiB;		//offset from the beginning of the file
	uint64_t stride = stoi(argv[7]) * kiB;		//stride between the blocks	
	uint64_t size = stoi(argv[3]) * kiB;			//number of bytes to be read, namely blocksize
	void *buffer = malloc(size + 10000);		//a pointer to a buffer big enough to hold the data, defined 10kiB larger than blocksize
	uint32_t bytesRead = 0;				//number of bytes actually read

	//declaring objects for files
	ofstream output_file(argv[2],ios::out);		//file object for output
	File *file = new File();			//File object to read
		
	//check if the file is opened
	file->Open(path, flags , mode, timeout);
	if (!file->IsOpen())
	{
		cout<<path<<" cannot be opened: No such file or directory"<<endl;
		return 0;
	}
	else 
		cout<<path<<" is opened for read"<<endl;

	if (!output_file.is_open())
	{
		cout<<argv[2]<<" cannot be created. Check if you are allowed to write"<<endl;
		return 0;
	}
	else
		cout<<argv[2]<<" is opened for write"<<endl;
	
	// this sequence is to force start measurements between 0th and 50th microsecond of each second wrt epoch. This way each clients will start within the same 50 microsecond interval of each second)
	auto start = high_resolution_clock::now();			//getting start time
	auto t =  duration_cast<microseconds>(system_clock::now().time_since_epoch());
        while (t.count()%1000000 > 50) t =  duration_cast<microseconds>(system_clock::now().time_since_epoch());
	
	//writing global variable of each run to the output file as the first line.	
	output_file<< size <<"\t"<< iteration<<"\t"<<path<<"\t"<<offset<<"\t"<<t.count()<<endl; 
	
	//iteration for read operations
	start = high_resolution_clock::now();			//getting start time
	while (i < iteration)
        {
                while((int)((duration_cast<microseconds>(high_resolution_clock::now() - start).count()) / 1000000) == second) // loop for 1 second interval
                {
				//stride = rand() % size;
				//offset+=size;
				//offset+=stride;
				//cout<<offset<<endl;
				//if (offset>=MAX_SIZE)
			
			//checking the file end before each read, if yes writes -1 to output file as the number of blocks read.
			if ((offset + (size + stride) * total_counter + size) >= MAX_SIZE)
			{
				output_file<<-1<<endl;
				counter = 0;
				break;

			}
              			//file->Read(offset, size, buffer, bytesRead, timeout);
              	
			file->Read(offset + (size + stride) * total_counter, size, buffer, bytesRead, timeout);
                        if (bytesRead != size) {cout<<"bytesRead is not equal to size"; continue;} 		//if bytesRead is not equal to blocksize, it skips and doesn't increase the counter. Anythng can be performed upon this exception. i.e. exit the program by giving an error
			total_counter++;
			counter++;
            	}                                                         
     	        second = (int)(duration_cast<microseconds>(high_resolution_clock::now() - start).count() / 1000000);
                output_file<<counter<<endl;				//writing only blocksize read in last second. Rest will be calculated during analysis
                counter = 0;
                i++;
     	}

	//closing files
	file->Close(timeout);
	output_file.close();
	if (!file->IsOpen())
        	cout<<path<<" is succesfully closed"<<endl;
	if (!output_file.is_open())
                cout<<argv[2]<<" is succesfully closed"<<endl;

	return 0;
}
